var app = new Vue({
    el: '#app',
    data: {
        product: 'Socks',
        image: './assets/vmSocks-green-onWhite.jpg',
        link: './assets/vmSocks-green-onWhite.jpg',
        alt: 'vmSocks-green-onWhite.jpg',
        description: 'Thin and Cotton Flouver',
        loadedTime: 'Image Loaded on ' + new Date().toLocaleString(),
        linkStyle: {
            'text-decoration': 'none',
            color: 'darkcyan'
        } 
    }
});

var app2 = new Vue({
    el: '#app-2',
    data: {
        seen: true,
        message: 'Now You See Me'
    } 
});

var app3 = new Vue({
    el: '#app-3',
    data: {
        numbers: [
            {text: 'One'},
            {text: 'Two'},
            {text: 'Three'},
            {text: 'Four'},
            {text: 'Five'},
        ]
    }
});

var app4 = new Vue({
    el: '#app-4',
    data: {
        seen: true,
        message: 'Loream Ipsum',
        btnName: 'Toggle Hide Message'
    },
    methods: {
        clickEvent: function () {
            if(this.seen){
                this.seen = false;
            } else {
                this.seen = true;
            }
        }
    }
});